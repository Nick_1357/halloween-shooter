﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework;
using System.Xml.Serialization;
using System.IO;

namespace HSGame
{
    public class HighScoreTracker
    {
        string fileName = "_HighScore_5p3c14l_name-so_nothing-bad_getsDELETED.txt";
        int highScore;

        FileStream fileDotText;
        
        StreamWriter writer;
        StreamReader reader;

        public void saveScore(int hiScore)
        {
            try
            {
                fileDotText = new FileStream(fileName, FileMode.Create, FileAccess.Write);
                writer = new StreamWriter(fileDotText);

                writer.WriteLine(("" + hiScore));

                writer.Close();
                fileDotText.Close();
            }
            catch
            {
                if(writer != null)
                {
                    writer.Close();
                }
                if(fileDotText != null)
                {
                    fileDotText.Close();
                }
            }
        }

        public int readScore()
        {
            try
            {
                fileDotText = new FileStream(fileName, FileMode.OpenOrCreate, FileAccess.Read);
                reader = new StreamReader(fileDotText);
                
                if (Int32.TryParse(reader.ReadLine(), out highScore))
                {
                    reader.Close();
                    fileDotText.Close();
                }
                else
                {
                    highScore = 0;
                }
                reader.Close();
                fileDotText.Close();
            }
            catch
            {
                if (reader != null)
                {
                    reader.Close();
                }
                if (fileDotText != null)
                {
                    fileDotText.Close();
                }
                highScore = 0;
                return highScore;
            }
            return highScore;
        }
    }
}
