﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HSGame
{
    public class Animator
    {
        private static Random r = new Random();
        public Rectangle sourceRect;
        Mob owner;

        private Point SheetFrames;//number of frames in the sprite sheet by width,height
        private Point frameSize;//the size of each frame in pixels, by width,height
        public Point thisFrame;//frame count coordinates of which frame is being displayed

        private int millSecPerFrame;
        private int millSecThisFrame;

        public Animator(Point frames, Point size, int msPerFrame, Mob o)
        {
            owner = o;
            SheetFrames = frames;
            frameSize = size;
            thisFrame = new Point(r.Next(frames.X), r.Next(frames.Y));

            sourceRect = new Rectangle(3, 0, frameSize.X, frameSize.Y);

            millSecPerFrame = msPerFrame;
            millSecThisFrame = 0;
        }

        public void update(int deltaTime)
        {
            if (millSecPerFrame == -1)//special case for directly controlling the animator
            {
                sourceRect.X = thisFrame.X * frameSize.X;
                sourceRect.Y = thisFrame.Y * frameSize.Y;
                return;//don't animate
            }

            millSecThisFrame += deltaTime;

            if(millSecThisFrame >= millSecPerFrame)
            {
                millSecThisFrame = 0;
                ++thisFrame.X;
                if(thisFrame.X >= SheetFrames.X)
                {
                    thisFrame.X = 0;
                    ++thisFrame.Y;
                    if(thisFrame.Y >= SheetFrames.Y)
                    {
                        thisFrame.Y = 0;
                    }
                }
                sourceRect.X = thisFrame.X * frameSize.X;
                sourceRect.Y = thisFrame.Y * frameSize.Y;

                //sourceRect.X = (int)MathHelper.Clamp(sourceRect.X, 0, (SheetFrames.X - 1) * frameSize.X);
                //sourceRect.Y = (int)MathHelper.Clamp(sourceRect.Y, 0, (SheetFrames.Y - 1) * frameSize.Y);
            }
        }//end of update

        public void forceFrameMove(int x, int y)//for grainular control over the animator
        {
            thisFrame.X += x;
            thisFrame.Y += y;

            thisFrame.X = (int)MathHelper.Clamp(thisFrame.X, 0, SheetFrames.X-1);
            thisFrame.Y = (int)MathHelper.Clamp(thisFrame.Y, 0, SheetFrames.Y-1);
        }
    }
}
