﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HSGame
{
    public class Pumpkin:Enemy
    {
        public Pumpkin(int xLoc, int yLoc, Texture2D sprite)
        {
            mySprite = sprite;

            posRect = new Rectangle(xLoc, yLoc, sprite.Width, sprite.Height);
            collisionBox = new Rectangle(posRect.X + 2, posRect.Y + 2, posRect.Width - 4, posRect.Height - 4);
            velocity = new Vector2(0, 1);

            anim = new Animator(new Point(1, 1), new Point(32, 32), -1, this);

            active = true;
            damage = 9;
            points = 110;
            attacking = false;
        }


        public override void removeSelf()
        {
            GameController.staticSelfPointer.pumpkins.Remove(this);
            GameController.staticSelfPointer.enemies.Remove(this);
            GameController.staticSelfPointer.mobs.Remove(this);
        }

        public override void attack(Player tgt)
        {
            target = tgt;
            velocity.Y = 5;
            attacking = true;
            int diff = tgt.collisionBox.Center.X - collisionBox.Center.X;

            if(Math.Abs(diff) < 1)
            {
                velocity.X = diff;
            }
            else
            {
                velocity.X = (diff > 0) ? 1 : -1;
            }

        }
    }
}
