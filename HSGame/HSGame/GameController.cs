﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Media;
using Microsoft.Xna.Framework.Audio;

namespace HSGame
{
    public class GameController
    {
        //Miscelaneous utility members
        public static GameController staticSelfPointer;
        public Random rng;
        public static Rectangle gameScreen;
        HighScoreTracker HiScore;
        int highScore;

        //level initialization info 
        public bool noLevel;
        public int level;
        int enemiesPerRow;
        int index;
        
        //player info is kept here instead of inside player. Player is more like a tool to change these variables
        int playerLives;
        int playerHealth;
        public int score;

        // THE player. this is the original reference. the player is constructed inside this class's constructor
        public Player player;

        //enemy behavior variables
        int milSecBetweenAttacks;
        int milSecSinceLastAttack;

        //textures    _*_ also used in level initialization _*_
        Texture2D[] backgrounds;
        Texture2D[] pumpkins1Tex;
        Texture2D[] pumpkins2Tex;
        Texture2D bat1Tex;
        Texture2D bat2Tex;
        Texture2D missileTex;

        //the hud reference, for calling draw hud...
        HUD hud;

        //active game enteties      _*_:::_*_ named and redundant to circumstep any need for sorting or type-checking _*_:::_*_
        public List<Mob> mobs;
        public List<Enemy> enemies;
        public List<Pumpkin> pumpkins;
        public List<Bat> bats;
        public List<Missile> missiles;

        //-------------------------------- SINGLETON CREATION -------------------------------- ** there is a public static self pointer too, but this can be called just to make sure it is not null afterwards **
        public static GameController createGameController(SpriteFont healthFont, SpriteFont infoFont, Texture2D plr, Texture2D[] p1, Texture2D[] p2, Texture2D b1, Texture2D b2, Texture2D mi, Texture2D[] bkgnds)
        {
            if(staticSelfPointer == null)
            {
                staticSelfPointer = new GameController(healthFont, infoFont, plr, p1, p2, b1, b2, mi, bkgnds);
            }
            return staticSelfPointer;
        }

        //-------------------------------- SINGLETON CONSTRUCTOR --------------------------------
        private GameController(SpriteFont healthFont, SpriteFont infoFont, Texture2D plr, Texture2D[] p1, Texture2D[] p2, Texture2D b1, Texture2D b2, Texture2D mi, Texture2D[] bkgnds)
        {
            rng = new Random();
            HiScore = new HighScoreTracker();
            highScore = HiScore.readScore();

            playerLives = 3;
            playerHealth = 100;
            score = 0;
            level = 0;// gets incrimented in the game1.Update() loop when a new level is made
            index = level;
            milSecBetweenAttacks = 2000;
            milSecSinceLastAttack = 0;

            pumpkins1Tex = p1;
            pumpkins2Tex = p2;
            bat1Tex = b1;
            bat2Tex = b2;
            missileTex = mi;
            backgrounds = bkgnds;

            gameScreen = new Rectangle(0, 0, 800, 600);
            enemiesPerRow = gameScreen.Width / 33;

            hud = new HUD(healthFont, infoFont, gameScreen.Width, gameScreen.Height, highScore);

            player = new Player(plr);

            mobs = new List<Mob>();
            enemies = new List<Enemy>();
            pumpkins = new List<Pumpkin>();
            bats = new List<Bat>();
            missiles = new List<Missile>();

            staticSelfPointer = this;
            noLevel = true;
        }

        //-------------------------------- UPDATE --------------------------------
        public void update(GameTime gameTime)
        {
            /* __Order of Events__
             * 
             * -check player status for death to reset player or to end the game
             * -select enemies and order to attack the player at steady intervals
             * -player.update() is called to move them first
             * -missile loop starts
             *   +inactive missiles are removed (usually off screen)
             *   +missile.update() is called to move
             * -enemy loop starts
             *   +inactive enemies are removed and skipped
             *   +enemy.update() is called to move 
             *   +enemy-missile collision is checked
             *   +enemy-player collision is checked
             * -if there are no more enemies, the flag is set to initialize the next level
            */

            if(!player.active)// checking/handling player death
            {
                if(playerLives == 0)
                {
                    MediaPlayer.Stop();
                    Game1.gamePlaying = false;
                    return;
                }
                --playerLives;
                playerHealth = 100;
                player.reset();
                player.active = true;
            }

            int deltaTime = gameTime.ElapsedGameTime.Milliseconds;
            milSecSinceLastAttack += deltaTime;

            if(milSecSinceLastAttack >= milSecBetweenAttacks)//enemies attack on a steady interval
            {
                milSecSinceLastAttack = 0;
                int pRand= rng.Next(2);//random left/right most pumpkin is chosen to attack 
                int bRand = rng.Next(bats.Count);// random bat is chosen to attack

                if(pumpkins.Count > 0)
                {
                    if(pRand == 0)
                    {
                        pumpkins[0].attack(player);
                    }
                    else
                    {
                        pumpkins[pumpkins.Count - 1].attack(player);
                    }
                }

                if(bats.Count > 0)
                {
                    bats[bRand].attack(player);
                }
            }

            player.update(deltaTime);//move player and check for death for resetting

            for(int i=missiles.Count-1; i>=0; i--)
            {
                if(!missiles[i].active)//cleaning up inactive missiles from last frame
                {
                    missiles[i].removeSelf();
                    continue;
                }
                missiles[i].update(deltaTime);//move missiles
            }
            
            for (int i = enemies.Count-1; i>=0; i--)
            {
                Enemy currentEnemy = enemies[i];

                if(!currentEnemy.active)//cleaning up inactive enemies from last frame
                {
                    currentEnemy.removeSelf();
                    continue;
                }
                currentEnemy.update(deltaTime);//move enemies

                for(int j = missiles.Count-1; j>= 0; j--)
                {
                    if(currentEnemy.collisionBox.Intersects(missiles[j].collisionBox))
                    {
                        score += currentEnemy.points;

                        SoundEffectInstance bang = Game1.explo.CreateInstance();
                        bang.Volume = 0.2f;
                        bang.Play();

                        currentEnemy.removeSelf();
                        missiles[j].removeSelf();
                        
                        goto NEXTENEMY;/////////////////////////////////////////////////// using goto for a continue on the outside loop
                    }                                                                   // to skip checking player collision on an enemy
                }                                                                       // a missile has destroyed.
                                                                                        //
                if(player.collisionBox.Intersects(currentEnemy.collisionBox))           //
                {                                                                       //
                    playerHealth -= currentEnemy.damage;                                //
                                                                                        //
                    SoundEffectInstance hit = Game1.playerHit.CreateInstance();         // other ways would not use a goto, but they are either
                    hit.Volume = 0.4f;                                                  // slightly logically different(see below), or add another 
                    hit.Play();                                                         // condition to the player collision for enemy.active, and 
                                                                                        // postpone the removal of the enemy object from the game.
                    currentEnemy.removeSelf();                                          //
                    if(playerHealth <=0)                                                //
                    {                                                                   //
                        player.active = false;                                          // this is to give the player the benefit of the 
                    }                                                                   // doubt in the edge cases where they fire a missile
                }                                                                       // to destroy an enemy on the same frame the enemy
            NEXTENEMY://////////////////////////////////////////////////////////////////// would collide with the player
                continue;//NEXTENEMY: needs to point to executable code

            }//////end of enemies loop//////

            if(enemies.Count == 0)
            {
                noLevel = true;//go to next level
            }
        }//end of update %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

        //-------------------------------- DRAW --------------------------------
        public void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(backgrounds[index], gameScreen, null, Color.White, 0, Vector2.Zero, SpriteEffects.None, 1);

            for(int i=0; i< mobs.Count; i++)
            {
                mobs[i].draw(spriteBatch);
            }
        }

        //-------------------------------- HUD --------------------------------
        public void drawHUD(SpriteBatch spriteBatch)
        {
            hud.display(playerHealth, score, playerLives, level, spriteBatch);
        }

        //-------------------------------- firing missiles --------------------------------
        public static void fireMissile(int x, int y)
        {
            Missile m = new Missile(x, y, staticSelfPointer.missileTex);
            staticSelfPointer.missiles.Add(m);
            staticSelfPointer.mobs.Add(m);
        }

        //-------------------------------- level initializiation --------------------------------
        public void initializeLevel(int level)
        {
            player.reset();
            if(mobs.Count > 0)
            {
                for(int i = mobs.Count - 1; i >= 0; i--)
                {
                    mobs[i].removeSelf();
                }
            }

            index = (level-1) & 3;//

            int enemyCount = (level << 1) +4;
            int rowsNeeded = (int)(( ((float)enemyCount)/enemiesPerRow ) + 0.9999f);//.9999 should round all numbers up for this domain
            
            int bottomRowCount = enemiesPerRow - (rowsNeeded * enemiesPerRow - enemyCount);
            int topRowY = gameScreen.Center.Y>>2;
            int gap = (enemiesPerRow - bottomRowCount) >> 1;

            int posX = 0;
            int posY = 0;

            int random;
            //the excessively deliberate order of enemy creation makes it easy to guarantee the attacking pumpkin is left/right -most, per cutomer requirement.
            for (int i = 0; i < enemiesPerRow && pumpkins.Count < enemyCount; i++)
            {
                posX = i * 33+6;
                for (int j = 0; j < rowsNeeded; j++)
                {
                    if (j == rowsNeeded - 1 && ( i < gap || enemiesPerRow-gap-1 < i ) ) break;//don't fill the short row completely

                    posY = topRowY + j * 64;
                    random = rng.Next(2);

                    pumpkins.Add(new Pumpkin(posX, posY, (random == 0) ? pumpkins1Tex[index] : pumpkins2Tex[index]));
                    bats.Add(new Bat(posX, posY - 32, (random == 0) ? bat1Tex : bat2Tex));
                }
            }

            enemies.AddRange(pumpkins);
            enemies.AddRange(bats);

            mobs.AddRange(enemies);
            mobs.Add(player);
            noLevel = false;
        }

        //-------------------------------- GAME RESET --------------------------------
        public void gameReset()
        {
            for(int i= mobs.Count-1; i>=0; i--)
            {
                mobs[i].removeSelf();
            }

            if(score > highScore)
            {
                highScore = score;
                HiScore.saveScore(score);
                hud.highScore = score;
            }

            playerLives = 3;
            playerHealth = 100;
            score = 0;
            level = 0;
            index = 0;
            milSecBetweenAttacks = 2000;
            milSecSinceLastAttack = 0;
            player.active = true;
        }
    }
}
