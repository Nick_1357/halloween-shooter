﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework;

namespace HSGame
{
    public class PauseScreen
    {
        string message = "    GAME PAUSED \nSTART or ESC resume";
        SpriteFont font;
        Texture2D tint;
        Rectangle screen;

        public PauseScreen(SpriteFont spF, Texture2D tex)
        {
            font = spF;
            tint = tex;
            screen = GameController.gameScreen;
        }

        public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(tint, screen, new Color(new Vector4(1,1,1,0.5f)) );
            spriteBatch.DrawString(font, message, new Vector2(screen.Center.X-170, screen.Center.Y-60), Color.Chartreuse);//What a controversial color.
        }
    }
}
