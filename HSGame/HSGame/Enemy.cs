﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace HSGame
{
    public abstract class Enemy:Mob
    {
        public int damage;
        public int points;
        protected bool attacking;
        protected Player target;
        protected int downCount;


        public abstract void attack(Player target);

        public override void update(int milliSecs)
        {
            posRect.X += (int)velocity.X;
            posRect.Y += (int)velocity.Y;

            collisionBox.X += (int)velocity.X;
            collisionBox.Y += (int)velocity.Y;

            anim.update(milliSecs);

            ++downCount;

            if(downCount > 20 && !attacking)//slowly stepping down the screen
            {
                velocity.Y = (velocity.Y == 1) ? 0 : 1;
                downCount = (velocity.Y == 0) ? -40 : 0;
            }

            if (!posRect.Intersects(GameController.gameScreen))
            {
                active = false;
            }

            if(attacking)
            {
                attack(target);
            }
        }
    }
}
