﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace HSGame
{
    public class Player:Mob
    {
        KeyboardState oldKBState;
        GamePadState oldGPState;
        int forceFrameWait;

        public Player(Texture2D sprite)
        {
            mySprite = sprite;
            posRect = new Rectangle(GameController.gameScreen.Width / 2 - 32, GameController.gameScreen.Height - sprite.Height - 8, 64, 64);
            collisionBox = new Rectangle(posRect.X + 4, posRect.Y + 4, posRect.Width - 8, posRect.Height - 8);
            velocity = new Vector2(0, 0);

            anim = new Animator(new Point(7, 1), new Point(64, 64), -1, this);/////////////////////////////////// different. needs to update based on controlls
            anim.forceFrameMove(3, 0);
            active = true;

            forceFrameWait = 0;
        }

        public override void update(int milliSecs)
        {
            ++forceFrameWait;
            int deltaX;
            getInput();
   
            if(forceFrameWait > 2)
            {
                forceFrameWait = 0;

                if(Math.Abs(velocity.X) < 2)//maneuvering left and right in the sprite frames so the spaceship looks like its flying
                {
                    velocity.X = 0;
                    deltaX = (anim.thisFrame.X == 3) ? 0 : ((anim.thisFrame.X - 3 > 0) ? -1 : 1);
                }
                else
                {
                    deltaX = (velocity.X > 0) ? 1 : -1;
                }

                anim.forceFrameMove(deltaX, 0);
            }


            anim.update(milliSecs);

            posRect.X += (int)velocity.X;
            posRect.Y += (int)velocity.Y;

            velocity.X = velocity.X * 0.93f;
            velocity.Y = velocity.Y * 0.8f;

            posRect.X = (int)MathHelper.Clamp(posRect.X, 0, GameController.gameScreen.Width - 64);
            posRect.Y = (int)MathHelper.Clamp(posRect.Y, 0, GameController.gameScreen.Height - 64);

            collisionBox.X = posRect.X + 3;
            collisionBox.Y = posRect.Y + 3;
        }

        public void getInput()
        {
            KeyboardState newKBState = Keyboard.GetState();
            GamePadState newGPState = GamePad.GetState(PlayerIndex.One);
            bool stickMove = true;

            if (Game1.gamePlaying)
            { 
                if ( newKBState.IsKeyDown(Keys.Escape) && oldKBState.IsKeyUp(Keys.Escape) || newGPState.IsConnected && newGPState.IsButtonDown(Buttons.Start) && oldGPState.IsButtonUp(Buttons.Start))
                {
                    Game1.gamePaused = !Game1.gamePaused;
                    if(MediaPlayer.State == MediaState.Playing)
                    {
                        MediaPlayer.Pause();
                    }
                    else
                    {
                        MediaPlayer.Resume();
                    }
                    
                }
            }
            else
            {
                if (newKBState.IsKeyDown(Keys.Escape) || newGPState.IsConnected && newGPState.IsButtonDown(Buttons.Start))
                {
                    //MediaPlayer.Play(Game1.myers);
                    Game1.gamePlaying = true;
                    GameController.staticSelfPointer.gameReset();
                }
            }

            if( newKBState.IsKeyDown(Keys.D) || newKBState.IsKeyDown(Keys.Right) || (newGPState.IsConnected && newGPState.IsButtonDown(Buttons.DPadRight)))//right
            {
                velocity.X += 2;
                stickMove = false;
            }
            if (newKBState.IsKeyDown(Keys.W) || newKBState.IsKeyDown(Keys.Up) || (newGPState.IsConnected && newGPState.IsButtonDown(Buttons.DPadUp)))//up
            {
                velocity.Y -= 2;
                stickMove = false;
            }
            if (newKBState.IsKeyDown(Keys.A) || newKBState.IsKeyDown(Keys.Left) || (newGPState.IsConnected && newGPState.IsButtonDown(Buttons.DPadLeft)))//left
            {
                velocity.X -= 2;
                stickMove = false;
            }
            if (newKBState.IsKeyDown(Keys.S) || newKBState.IsKeyDown(Keys.Down) || (newGPState.IsConnected && newGPState.IsButtonDown(Buttons.DPadDown)))//down
            {
                velocity.Y += 2;
                stickMove = false;
            }

            if(newGPState.IsConnected && stickMove)
            {
                velocity.X += (int)(newGPState.ThumbSticks.Left.X * 5);
                velocity.Y -= (int)(newGPState.ThumbSticks.Left.Y * 4);
            }

            if(Math.Abs(velocity.X) > 10 || Math.Abs(velocity.Y) > 10)
            {
                velocity.X = (int)MathHelper.Clamp(velocity.X, -10f, 10.1f);
                velocity.Y = (int)MathHelper.Clamp(velocity.Y, -10.1f, 10.1f);
            }
            
            if( (oldKBState.IsKeyUp(Keys.Space) && newKBState.IsKeyDown(Keys.Space)) ||
                (newGPState.IsConnected && (oldGPState.IsButtonUp(Buttons.RightShoulder) && newGPState.IsButtonDown(Buttons.RightShoulder))) )
            {
                GameController.fireMissile(posRect.Center.X, posRect.Y );
            }

            oldKBState = newKBState;
            oldGPState = newGPState;
        }//end of getInput()

        public override void removeSelf()
        {
            GameController.staticSelfPointer.mobs.Remove(this);
        }

        public void reset()
        {
            posRect.X = GameController.gameScreen.Width / 2 - 32;
            posRect.Y = GameController.gameScreen.Height - mySprite.Height - 8;
        }
    }
}
