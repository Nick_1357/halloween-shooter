﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HSGame
{
    public class Missile:Mob
    {
        public Missile(int xLoc, int yLoc, Texture2D sprite)
        {
            mySprite = sprite;

            velocity = new Vector2(0, -10);// 10pixels/frame upward
            posRect = new Rectangle(xLoc - 12, yLoc - 24, 24, 32);
            collisionBox = new Rectangle(posRect.X, posRect.Y, posRect.Width, posRect.Height);

            anim = new Animator(new Point(5, 8), new Point(32, 32), 1, this);

            active = true;
        }

        public override void removeSelf()
        {
            GameController.staticSelfPointer.missiles.Remove(this);
            GameController.staticSelfPointer.mobs.Remove(this);
        }
    }
}
