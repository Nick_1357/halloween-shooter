﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HSGame
{
    public class GameOverScreen
    {
        string message = "\n\n\n\n\n                  GAME OVER \n           START or ESC to restart";
        SpriteFont font;
        Texture2D tint;
        Rectangle screen;

        public GameOverScreen(SpriteFont spF, Texture2D tex)
        {
            font = spF;
            tint = tex;
            screen = GameController.gameScreen;
        }

        public void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(tint, screen, Color.White);
            spriteBatch.DrawString(font, "                           score:   " + GameController.staticSelfPointer.score + message, new Vector2(screen.Center.X-380, screen.Center.Y-290), Color.OrangeRed);
        }
    }
}
