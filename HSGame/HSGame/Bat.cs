﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HSGame
{
    public class Bat:Enemy
    {
        int sonarRangeSQR = 40000;
        AI myAI = new AI();

        //constructor
        public Bat(int xLoc, int yLoc, Texture2D sprite)
        {
            mySprite = sprite;

            posRect = new Rectangle(xLoc, yLoc, 32, sprite.Height);
            collisionBox = new Rectangle(posRect.X + 1, posRect.Y + 4, posRect.Width - 2, posRect.Height - 8);
            velocity = new Vector2(0, 1);

            anim = new Animator(new Point(3, 1), new Point(32, 32), 160, this);

            active = true;
            damage = 14;
            points = 355;
            attacking = false;
        }
        
        //utility
        int distToPlayerSQR()
        {
            int x = (collisionBox.Center.X - GameController.staticSelfPointer.player.collisionBox.Center.X);
            int y = (collisionBox.Center.Y - GameController.staticSelfPointer.player.collisionBox.Center.Y);
            return x * x + y * y;
        }

        //update
        public override void update(int milliSecs)
        {
            float distSQR = distToPlayerSQR();

            posRect.X += (int)velocity.X;
            posRect.Y += (int)velocity.Y;

            collisionBox.X += (int)velocity.X;
            collisionBox.Y += (int)velocity.Y;

            anim.update(milliSecs);

            if (!posRect.Intersects(GameController.gameScreen))
            {
                active = false;
                return;
            }

            if (distSQR < sonarRangeSQR)//in range to lock on
            {
                if (!attacking)
                {
                    attack(GameController.staticSelfPointer.player);
                }
                myAI.seekAndDestroy(this, GameController.staticSelfPointer.player);
                return;
            }
            else if (attacking)//lost range: proceed with default attack
            {
                velocity.Y = 3;
                attack(target);
            }

            ++downCount;
                
            if (downCount > 20 && !attacking)
            {
                velocity.Y = (velocity.Y == 1) ? 0 : 1;
                downCount = (velocity.Y == 0) ? -40 : 0;
            }
        }

        //sorta destructor
        public override void removeSelf()
        {
            GameController.staticSelfPointer.bats.Remove(this);
            GameController.staticSelfPointer.enemies.Remove(this);
            GameController.staticSelfPointer.mobs.Remove(this);
        }


        public override void attack(Player tgt)
        {
            if(!attacking)
            {
                target = tgt;
                velocity.Y = 3;
                attacking = true;
                sonarRangeSQR = 160000;
            }
            
            int diff = tgt.collisionBox.Center.X - collisionBox.Center.X;

            if (Math.Abs(diff) < 2)
            {
                velocity.X = diff;
            }
            else
            {
                velocity.X = (diff > 0) ? 2 : -2;
            }
        }
    }
}
