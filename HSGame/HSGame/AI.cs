﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;

namespace HSGame
{
    public class AI
    {
        float maxTurnRate = 0.05f;
        float maxSpeed = 5f;
        
        public void seekAndDestroy(Bat owner, Mob target)
        {
            Vector2 ownerCent = new Vector2(owner.collisionBox.Center.X, owner.collisionBox.Center.Y);
            Vector2 targetCent = new Vector2(target.collisionBox.Center.X, target.collisionBox.Center.Y);

            Vector2 tarVel = targetCent - ownerCent;//relative vector pointing from owner to target
            tarVel.Normalize();

            Vector2 curVel = owner.velocity;
            if (curVel.X == 0 && curVel.Y == 0)
            {
                curVel.Y = 1;//making sure not to divide by zero
            }
            else
            {
                curVel.Normalize();
            }


            float tar = (float)Math.Atan2(tarVel.Y, tarVel.X);
            float cur = (float)Math.Atan2(curVel.Y, curVel.X);
            float rot = tar - cur;
            
            rot = MathHelper.Clamp(examplesWrapAngle(rot), -maxTurnRate, maxTurnRate);//using example function

            tarVel.X = (float)Math.Cos(examplesWrapAngle(cur + rot)) * maxSpeed;
            tarVel.Y = (float)Math.Sin(examplesWrapAngle(cur + rot)) * maxSpeed;

            owner.velocity = tarVel;
        }

        private float examplesWrapAngle(float angle)//example function from from msdn.microsoft.com -- angles are wrapped to +/-Math.PI
        {
            while (angle > MathHelper.Pi)
            {
                angle -= MathHelper.TwoPi;
            }
            while (angle < -MathHelper.Pi)
            {
                angle += MathHelper.TwoPi;
            }
            return angle;
        }
    }
}
