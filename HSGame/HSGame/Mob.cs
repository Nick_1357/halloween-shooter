﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HSGame
{
    public abstract class Mob
    {
        protected Rectangle posRect;
        public Rectangle collisionBox;
        public Vector2 velocity;
        protected Texture2D mySprite;
        protected Animator anim;

        public bool active;

        public virtual void update(int milliSecs)
        {
            posRect.X += (int)velocity.X;
            posRect.Y += (int)velocity.Y;

            collisionBox.X += (int)velocity.X;
            collisionBox.Y += (int)velocity.Y;

            anim.update(milliSecs);

            if(!posRect.Intersects(GameController.gameScreen))
            {
                active = false;
            }
        }

        public virtual void draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(mySprite, posRect, anim.sourceRect, Color.White, 0, Vector2.Zero, SpriteEffects.None, 0.5f);
        }

        public abstract void removeSelf();

    }
}
