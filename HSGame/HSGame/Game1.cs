using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace HSGame
{
    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Game1 : Microsoft.Xna.Framework.Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        SpriteFont healthFont;
        SpriteFont infoFont;

        //Vector2 welcomeMsgPos;
        //Vector2 welcomeShadow;

        Texture2D[] backgrounds;
        Texture2D[] pumpkins1;
        Texture2D[] pumpkins2;
        Texture2D bat1;
        Texture2D bat2;

        Texture2D miss;
        Texture2D player;
        Texture2D psTint;

        GameController GC;
        PauseScreen pauseScreen;
        GameOverScreen GOScreen;

        public static SoundEffect explo;
        public static SoundEffect playerHit;
        
        public static Song myers;

        public static bool gamePlaying = true;
        public static bool gamePaused = false;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";

            graphics.PreferredBackBufferWidth = 800;
            graphics.PreferredBackBufferHeight = 600;

            backgrounds = new Texture2D[4];
            pumpkins1 = new Texture2D[4];
            pumpkins2 = new Texture2D[4];
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();

            //IsFixedTimeStep = true;
            //TargetElapsedTime = TimeSpan.FromMilliseconds(400);
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            psTint = Content.Load<Texture2D>(@"images/1x1BlackPixel");

            //image credits: http://www.pixelstalk.net/halloween-backgrounds-free-download/
            backgrounds[0] = Content.Load<Texture2D>(@"images/Free-download-Halloween-Backgrounds800x600");
            backgrounds[1] = Content.Load<Texture2D>(@"images/Halloween-HD-Wallpapers800x600");
            backgrounds[2] = Content.Load<Texture2D>(@"images/Halloween-Images800x600");
            backgrounds[3] = Content.Load<Texture2D>(@"images/Halloween-Laptop-Backgrounds800x600");

            pumpkins1[0] = Content.Load<Texture2D>(@"images/fdhbPumpkin1");
            pumpkins1[1] = Content.Load<Texture2D>(@"images/hhdwPumpkin1");
            pumpkins1[2] = Content.Load<Texture2D>(@"images/hiPumpkin1");
            pumpkins1[3] = Content.Load<Texture2D>(@"images/hlbPumpkin1");
            pumpkins2[0] = Content.Load<Texture2D>(@"images/fdhbPumpkin2");
            pumpkins2[1] = Content.Load<Texture2D>(@"images/hhdwPumpkin2");
            pumpkins2[2] = Content.Load<Texture2D>(@"images/hiPumpkin2");
            pumpkins2[3] = Content.Load<Texture2D>(@"images/hlbPumpkin2");

            //bat1 credit: bagzie on http://opengameart.org/
            //bat2 credit: XenosNS http://opengameart.org/
            bat1 = Content.Load<Texture2D>(@"images/32x32 bat-sprite_modified");
            bat2 = Content.Load<Texture2D>(@"images/sBat_strip4_modified");

            //player credit: Lamoot on http://opengameart.org/
            player = Content.Load<Texture2D>(@"images/fighter");

            //missile credit: Krasi Wasilev ( http://freegameassets.blogspot.com) on http://opengameart.org/
            miss = Content.Load<Texture2D>(@"images/Air_fighter01_set_32");

            //explosion credit: https://www.freesoundeffects.com/free-sounds/explosion-10070/
            explo = Content.Load<SoundEffect>(@"audio/Explosion");
            playerHit = Content.Load<SoundEffect>(@"audio/Explosion+7");

            //turns out this song is in the same bucket as the happy birthday song.
            //Halloween (1978) theme music credit: written by John Carpenter - (cut and edited for this project)
            //myers = Content.Load<Song>(@"audio/hallow");
            //MediaPlayer.Play(myers);
            //MediaPlayer.IsRepeating = true;

            healthFont = Content.Load<SpriteFont>(@"SpriteFont2");
            infoFont = Content.Load<SpriteFont>(@"SpriteFont1");

            GC = GameController.createGameController(healthFont, infoFont, player, pumpkins1, pumpkins2, bat1, bat2, miss, backgrounds);
            pauseScreen = new PauseScreen(healthFont, psTint); // this MUST be constructed AFTER the GameController
            GOScreen = new GameOverScreen(healthFont, psTint); // this MUST be constructed AFTER the GameController



            // TODO: use this.Content to load your game content here
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            //if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
            //this.Exit();

            // TODO: Add your update logic here

            if(GC.noLevel)
            {
                GC.initializeLevel(++GC.level);
            }

            if(!gamePaused && gamePlaying)
            {
                GC.update(gameTime);
            }
            
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();
            if(gamePlaying)
            {
                GC.Draw(spriteBatch);
                GC.drawHUD(spriteBatch);

                if(gamePaused)
                {
                    pauseScreen.draw(spriteBatch);
                    GameController.staticSelfPointer.player.getInput();
                }
            }
            else
            {
                GOScreen.draw(spriteBatch);
                GameController.staticSelfPointer.player.getInput();
            }
            

            spriteBatch.End();

            // TODO: Add your drawing code here

            base.Draw(gameTime);
        }
    }
}
