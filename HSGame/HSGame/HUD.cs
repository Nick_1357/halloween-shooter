﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace HSGame
{
    public class HUD
    {
        SpriteFont healthFont;
        SpriteFont infoFont;

        Vector2 posHealth;
        Vector2 posScore;
        Vector2 posLives;
        Vector2 posLevel;
        Vector2 posHiScore;

        public int highScore;

        public HUD(SpriteFont healthF, SpriteFont infoF, int scrWidth, int scrHeight, int hiScore)
        {
            healthFont = healthF;
            infoFont = infoF;
            posHealth = new Vector2(4, -4);
            posScore = new Vector2(scrWidth - 210, 0);
            posLives = new Vector2(4, scrHeight - 28);
            posLevel = new Vector2(scrWidth - 100, scrHeight - 28);
            posHiScore = new Vector2(posScore.X - 250, 0);
            highScore = hiScore;
        }

        public void display(int h, int s, int li, int le, SpriteBatch spriteBatch)
        {
            spriteBatch.DrawString(healthFont, "HEALTH " + h, posHealth, Color.DarkRed);
            spriteBatch.DrawString(infoFont, "SCORE " + s, posScore, Color.WhiteSmoke);
            spriteBatch.DrawString(infoFont, "LIVES " + li, posLives, Color.NavajoWhite);
            spriteBatch.DrawString(infoFont, "LEVEL " + le, posLevel, Color.NavajoWhite);
            spriteBatch.DrawString(infoFont, "HIGH SCORE " + highScore, posHiScore, Color.LightGoldenrodYellow);
        }
    }
}
