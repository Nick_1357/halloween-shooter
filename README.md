Environment developed in: Visual Studio 2015, XNA 4.0 Framework

Controls -
    Keyboad: arrow keys / space
    Gamepad: d-pad / left stick / right shoulder(R1)
    (most USB controllers)

If you are going to read the code, then I recommend starting in Game1.cs and going back and forth between GameController.cs. The others can be described as appendages.
Game1.cs is where the content is pipe lined in, and at the bottom are the Update()
and Draw() loops, which are called each frame.